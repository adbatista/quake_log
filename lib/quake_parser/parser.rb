module QuakeParser
  class Parser
    REGEX = {
      :INIT_GAME     => /InitGame:/,
      :END_GAME      => /ShutdownGame:/,
      :ADD_PLAYER    => /ClientUserinfoChanged:\s([0-9])\sn\\([a-zA-Z0-9 ]+)/,
      :KILL          => /Kill:\s([0-9]+)\s([0-9]+)\s([0-9]+):/
    }

    attr_reader :games

    def initialize
      @games = []
      @game_state = GameState.new
    end

    def parse(file)
      while line = file.gets
        if @game_state.ready? && line =~ REGEX[:INIT_GAME]
          init
        elsif @game_state.running?
          game_running_actions(line)
        end
      end
      self
    end

    def to_h
      @games.each_with_index.map { |game,index|  ["game_#{index+1}", Hash(game)] }.to_h
    end

    private

    def game_running_actions(line)
      case line
      when REGEX[:END_GAME]
        finish
      when REGEX[:ADD_PLAYER]
         @current_game.add_player($1,$2)
      when REGEX[:KILL]
        @current_game.kill_player killer: $1, killed: $2, mean: $3
      end
    end

    def init
      @current_game = Game.new
      @game_state.run
    end

    def finish
      @game_state.finish
      @games << @current_game
    end
  end
end
