module QuakeParser
  class Game
    WORLD_ID = 1022

    attr_reader :total_kills

    def initialize
      @total_kills = 0
      @players= Hash.new { |hash, key| hash[key] = {} }
      @kill_reasons = Hash.new(0)
    end

    def add_player(id, name)
      id = normalize_key(id)
      @players[id][:name] = name
      @players[id].merge! kills: 0 do |_key, current_kills, initial_kills|
        current_kills || initial_kills
      end
    end

    def kill_player(killer:, killed:,mean:)
      @total_kills += 1

      add_kills(killer, killed)

      add_mean(mean)
    end

    def kills_by_means
      @kill_reasons.map { |id, kills|  [KILL_REASON[id], kills] }.to_h
    end

    def players
      @players.map { |_id,player| player[:name] }
    end

    def kills
      @players.map { |_id,player| player.values }.to_h
    end

    def to_hash
      {
        total_kills: @total_kills,
        players: players,
        kills: kills,
        kills_by_means: kills_by_means
      }
    end

    private

    def normalize_key(key)
      Integer(key)
    end

    def add_mean(mean)
      mean = normalize_key(mean)
      @kill_reasons[mean] += 1
    end

    def add_kills(killer,killed)
      killer = normalize_key(killer)
      killed = normalize_key(killed)

      if killer == WORLD_ID
        @players[killed][:kills] -= 1
      else
        @players[killer][:kills] +=1
      end
    end
  end
end
