module QuakeParser
  class GameState
    def initialize
      @state = :ready
    end

    def run
      @state = :running
    end

    def finish
      @state = :ready
    end

    def ready?
      @state == :ready
    end

    def running?
      @state == :running
    end
  end
end
