require 'pp'
module QuakeParser
  class Report
    def initialize(log)
      @games_data = Parser.new.parse(File.open(log,'r')).to_h
    end

    def report
      puts '===================== Games Result ====================='
      pp @games_data
      puts '======================= Ranking ========================'
      puts players_ranking
    end

    def players_ranking
      ranking = calculate_kills.sort_by {|_player,kills| -kills}

      ranking.map {|value| value.join(' => ')}
    end

    private
    def calculate_kills
      sum_kills = Hash.new(0)

      @games_data.map do |_id,game|
        game[:kills].each do |player,kills|
          sum_kills[player] += kills
        end
      end
      sum_kills
    end
  end
end
