require "spec_helper"
module QuakeParser
  describe Parser do
    context "game start and end" do
      context "when game start and finish" do
        let(:log) do
          StringIO.new '
           0:00 ------------------------------------------------------------
           0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0
          20:37 ShutdownGame:
          20:37 ------------------------------------------------------------
           0:00 ------------------------------------------------------------
           0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0
          20:37 ShutdownGame:
          20:37 ------------------------------------------------------------
          '
        end

        it "when game start and finish","add games" do
          subject.parse log
          expect(subject).to have(2).games
        end
      end

      context "when game only start" do
        let(:log) do
          StringIO.new '
          0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0
          '
        end
        it "not add game" do
          subject.parse log
          expect(subject).to have(0).games
        end
      end

      context "when game not start" do
        let(:log){ StringIO.new '20:37 ShutdownGame:' }

        it "not add game" do
          subject.parse log
          expect(subject).to have(0).games
        end
      end
    end

    context "adding user to the game" do
      let(:log) do
        StringIO.new '
         0:00 ------------------------------------------------------------
         0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0
        20:34 ClientConnect: 2
        20:34 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\xian/default\hmodel\xian/default\g_redteam\\g_blueteam\\c1\4\c2\5\hc\100\w\0\l\0\tt\0\tl\0
        20:37 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\uriel/zael\hmodel\uriel/zael\g_redteam\\g_blueteam\\c1\5\c2\5\hc\100\w\0\l\0\tt\0\tl\0
        20:37 ClientBegin: 2
        20:37 ShutdownGame:
        20:37 ------------------------------------------------------------
        '
      end

      it "add_player" do
        expect_any_instance_of(Game).to receive(:add_player).at_least(1)

        subject.parse log
      end
    end

    context "player kill" do
      let(:log) do
        StringIO.new '
         20:37 ------------------------------------------------------------
         20:37 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\bot_minplayers\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0
         20:38 ClientConnect: 2
         20:38 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\uriel/zael\hmodel\uriel/zael\g_redteam\\g_blueteam\\c1\5\c2\5\hc\100\w\0\l\0\tt\0\tl\0
         20:38 ClientBegin: 2
         20:39 ClientConnect: 3
         20:39 ClientUserinfoChanged: 3 n\Dono da Bola\t\0\model\sarge/krusade\hmodel\sarge/krusade\g_redteam\\g_blueteam\\c1\5\c2\5\hc\95\w\0\l\0\tt\0\tl\0
         20:40 ClientBegin: 3
         20:54 Kill: 3 2 22: Dono da Bola killed Isgalamido by MOD_TRIGGER_HURT
         26:10 ShutdownGame:
         26:10 ------------------------------------------------------------
        '
      end

      it "kill" do
        expect_any_instance_of(Game).to \
          receive(:kill_player).with(killer: '3', killed: '2', mean: '22')

        subject.parse log
      end
    end

    describe "#to_h" do
      let(:log) do
        StringIO.new '
         20:37 ------------------------------------------------------------
         20:37 InitGame: \sv_floodProtect\1\sv_maxPing\0\sv_minPing\0\sv_maxRate\10000\sv_minRate\0\sv_hostname\Code Miner Server\g_gametype\0\sv_privateClients\2\sv_maxclients\16\sv_allowDownload\0\bot_minplayers\0\dmflags\0\fraglimit\20\timelimit\15\g_maxGameClients\0\capturelimit\8\version\ioq3 1.36 linux-x86_64 Apr 12 2009\protocol\68\mapname\q3dm17\gamename\baseq3\g_needpass\0
         20:38 ClientConnect: 2
         20:38 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\uriel/zael\hmodel\uriel/zael\g_redteam\\g_blueteam\\c1\5\c2\5\hc\100\w\0\l\0\tt\0\tl\0
         20:38 ClientBegin: 2
         20:39 ClientConnect: 3
         20:39 ClientUserinfoChanged: 3 n\Dono da Bola\t\0\model\sarge/krusade\hmodel\sarge/krusade\g_redteam\\g_blueteam\\c1\5\c2\5\hc\95\w\0\l\0\tt\0\tl\0
         20:40 ClientBegin: 3
         20:54 Kill: 3 2 22: Dono da Bola killed Isgalamido by MOD_TRIGGER_HURT
         26:10 ShutdownGame:
         26:10 ------------------------------------------------------------
        '
      end

      it "return a hash with all games data" do
        expectation = {
          "game_1" => {
            total_kills:1,
            players:['Isgalamido', 'Dono da Bola'],
            kills:{
              'Isgalamido' => 0,
              'Dono da Bola' => 1
            },
            kills_by_means: {
              "MOD_TRIGGER_HURT" => 1
            }
          }
        }

        subject.parse log
        expect(subject.to_h).to eq expectation
      end
    end
  end
end
