require "spec_helper"
module QuakeParser
  describe Report do
    let(:log) { File.join(__dir__,'fixtures','game.log') }

    subject { Report.new(log) }
    describe "#players_ranking" do
      it "show killers ordered by kills" do
        expectation = ["Isgalamido => 2", "Dono da Bola => -2", "Zeh => -4"]

        expect(subject.players_ranking).to eq expectation
      end
    end

    describe "#report" do
      around(:example) do |example|
        old_stdout, $stdout = $stdout, StringIO.new
        example.call
        $stdout = old_stdout
      end

      it "show report" do
        subject.report
        $stdout.rewind
        expect($stdout.read).to eq <<-REPORT
===================== Games Result =====================
{"game_1"=>
  {:total_kills=>4,
   :players=>["Dono da Bola", "Isgalamido", "Zeh"],
   :kills=>{"Dono da Bola"=>-1, "Isgalamido"=>1, "Zeh"=>-2},
   :kills_by_means=>
    {"MOD_ROCKET"=>1, "MOD_TRIGGER_HURT"=>2, "MOD_FALLING"=>1}},
 "game_2"=>
  {:total_kills=>4,
   :players=>["Dono da Bola", "Isgalamido", "Zeh"],
   :kills=>{"Dono da Bola"=>-1, "Isgalamido"=>1, "Zeh"=>-2},
   :kills_by_means=>
    {"MOD_ROCKET"=>1, "MOD_TRIGGER_HURT"=>2, "MOD_FALLING"=>1}}}
======================= Ranking ========================
Isgalamido => 2
Dono da Bola => -2
Zeh => -4
        REPORT
      end
    end
  end
end
