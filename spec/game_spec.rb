require "spec_helper"
module QuakeParser
  describe Game do
    describe "#add_player" do
      before do
        subject.add_player(2,'Mario')
      end

      it "add new player" do
        expect(subject.players).to eq ['Mario']
      end

      it "player can change name" do
        subject.add_player(2, 'Luigi')
        expect(subject.players).to eq ['Luigi']
      end
    end

    context 'kills' do
      before  do
        subject.add_player 2, 'Mario'
        subject.add_player 3, 'Bowser'
      end

      describe "#kill_player" do
        it "increase total kills" do
          expect{
            subject.kill_player killer: 2, killed: 3, mean: 7
          }.to change{ subject.total_kills}.by 1
        end

        it "when player kills another player", "increase player kills" do
          expect {
            subject.kill_player killer: 2, killed: 3, mean: 7
          }.to change {subject.kills['Mario'] }.by(1)

          subject.kills
        end

        it 'when world kills a player', "decrease killed player kills" do
          expect{
            subject.kill_player killer: Game::WORLD_ID, killed: 2, mean: 22
          }.to change{ subject.kills['Mario'] }.by(-1)
        end

        it "when player doesn't kill nobody", "player have 0 kills" do
          expect(subject.kills['Bowser']).to be_zero
        end
      end

      describe "#kills_by_means" do
        it "return the reason of kill and how many times occurred" do
          subject.kill_player killer: 2, killed: 3, mean: 7
          subject.kill_player killer: 2, killed: 3, mean: 7
          subject.kill_player killer: Game::WORLD_ID , killed: 3, mean: 22

          expectation = { 'MOD_ROCKET_SPLASH' => 2, 'MOD_TRIGGER_HURT' => 1 }
          expect(subject.kills_by_means).to eq expectation
        end
      end
    end

    describe "#to_hash" do
      before do
        subject.add_player 2, 'Mario'
        subject.add_player 3, 'Bowser'
        subject.kill_player killer: 2, killed: 3, mean: 7
        subject.kill_player killer: Game::WORLD_ID, killed: 3, mean: 22
      end

      it "return a hash with game data" do
        expectation = {
          total_kills: 2,
          players: ['Mario','Bowser'],
          kills: {
            'Mario'  => 1,
            'Bowser' => -1
          },
          kills_by_means: {
            'MOD_ROCKET_SPLASH' => 1,
            'MOD_TRIGGER_HURT'  => 1
          }
        }
        expect(subject.to_hash).to eq expectation
      end
    end
  end
end
