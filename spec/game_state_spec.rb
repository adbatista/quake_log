require 'spec_helper'
module QuakeParser
  describe GameState do
    it { is_expected.to be_ready }

    it "#run" do
       subject.run
       expect(subject).to be_running
    end

    it "#finish" do
       subject.finish
       expect(subject).to be_ready
    end
  end
end
